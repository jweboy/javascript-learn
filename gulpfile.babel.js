import gulp from 'gulp'
import browserSync from 'browser-sync'

// 静态服务器
gulp.task('server', function() {
    browserSync.init({
        server: {
            baseDir: "./"
        },
        reloadOnRestart: true,
        watch: true
    })
})