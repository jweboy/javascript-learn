// 发布订阅模式，也成观察者模式，定义对象间的一种一对多的依赖关系

function EventEmitter() {
    this.events = {}
}

EventEmitter.prototype = {
    on(name, cb) {
        if(!this.events[name]) {
            this.events[name] = []
        }
        this.events[name].push(cb)
    },
    emit(name) {
        const handler = this.events[name]
        if(typeof handler === 'function') {
            handler.apply(this, arguments)
        }
    },
    remove(name, listener) {
        const handler = this.events[name]
        if(Array.isArray(handler)) {
            while(~handler.indexOf(listener)) {
                const index = handler.findIndex(listener)
                handler.splice(index, 1)
            }
        } else if(handler === listener){
            delete this.events[name]
        }
    }
}

const pubsub = new EventEmitter()
pubsub.on('string', 'hello world')
pubsub.emit('func', function(val) {
    console.log(val)
})
pubsub.on('func', function() {
    return 'func'
})
// console.log(pubsub.events);