/**
 * 同作用域中let、const不可重复声明
 * let、const不会声明到全局作用域
 * let、const不会变量提升
 */
{
    let a = 1
    const b = 2
}
{
    let a = 3
    const b = 4
}
let a = 5
const b = 7