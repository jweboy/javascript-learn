// Object.create()方法的具体实现过程
function objectCreate(obj) {
    function F() {}
    F.prototype = obj
    return new F()
}