// 参考 
// https://juejin.im/post/5a96d78ef265da4e9311b4d8?utm_medium=fe&utm_source=weixinqun#heading-2
// https://segmentfault.com/a/1190000002440502#articleHeader10

// js有两种继承方式
// 原型链继承（对象间的继承）-> 借助已有对象创建新对象，并将子类的原型指向父类
// 类式继承（构造函数间的继承）-> 子类构造函数调用超类构造函数 function Sub() { Super.call(this) }

{
    /**
     * 原型继承
     */
    const parent = {
        name: 'bill',
        friends: ['one', 'two', 'three']
    }
    const child = Object.create(parent)
    // console.dir(child)
}

{
    /**
     * 1.原型链继承
     * @description - 缺点
     * - 字面量重写原型会中断
     * - 子类无法给父类传递动态参数
     */
    function Parent() {
        this.name = 'jweboy'
    }
    function Child() {
        this.age = 22
    }
    Child.prototype = new Parent()
    const child = new Child()

    // console.dir(child)
    // console.log(Child.prototype.isPrototypeOf(child))
    // console.log(child instanceof Parent)
}

{
    /**
     * 2.构造函数继承
     * @param {String} username 
     * @param {String} password 
     * @description 解决原型链继承的缺点，但是它没有继承父类原型的方法
     */
    function Parent(username, password) {
        this.password = password
        this.username = username
    }

    Parent.prototype = {
        login() {
            console.log(`login as , ${this.username}, password is ${this.password}.`)
        }
    }

    function Child(username, password) {
        Parent.call(this, username, password)
        this.articles = 30
    }

    const child = new Child('jweboy', 'jl940630')
    // child.login() // error: is not a function
    // console.log(child.articles)
    // console.log(child.__proto__)
    // console.dir(child)
}

{
    /**
     * 组合继承
     * @param {String} username 
     * @param {String} password 
     * @description - 缺点
     *  - 子类无法动态给父类传递参数
     *  - 父类构造函数被调用两次
     */
    function Parent(username, password) {
        this.password = password
        this.username = username
    }

    Parent.prototype = {
        login() {
            console.log(`login as , ${this.username}, password is ${this.password}.`)
        }
    }

    function Child(username, password) {
        Parent.call(this, username, password) // 第二次调用
        this.articles = 30
    }
    Child.prototype = new Parent() // 第一次调用
    // Child.prototype = Object.create(Parent.prototype) // 这种做法能够避免当前继承方式存在的问题

    const user = new Child('jweboy', 'jl940630')
    // user.login()
    // console.log(user.articles)
    // console.dir(user)
}

{
    /**
     * 寄生继承 (原型继承 + 工厂模式)
     *
     * @param {Object} obj
     * @returns Object
     */
    function createObject(obj) {
        const _obj = Object.create(obj)
        _obj.run = function() { return 'run' }
        return _obj
    }
    const obj = createObject({ name: 'tao' })
    // console.dir(obj)
}

{
    function inherit(child, parent) {
        // 继承父类原型
        const prototype = Object.create(parent.prototype)
        // 将父类与子类原型合并并赋值给子类原型
        // 这种方法有一定缺陷：1.只适用于copy原型链上可枚举的方法 2.如果子类本身已经继承其他类那么这个继承将不满足
        child.prototype = Object.assign(prototype, child.prototype)
        // 重写被污染的子类的constructor
        prototype.constructor = child
    }

    /**
     * 3.组合寄生继承
     * @param {String} username
     * @param {String} password
     * @description - 优点
     *  - 子类可以动态传递参数给父类
     *  - 父类的构造函数只执行一次
     *  - 子类属性创建在各自的原型链，子类之间不共享属性
     */
    function Parent(username, password) {
        this.password = password
        this.username = username
    }

    Parent.prototype.login = function() {
        console.log(`login as , ${this.username}, password is ${this.password}.`)
    }

    function Child(username, password) {
        Parent.call(this, username, password) // 属性继承
        this.articles = 30
    }

    // 继承
    inherit(Child, Parent)

    Child.prototype.read = function() {
        console.log('read article at', this.articles)
    }

    const user = new Child('jweboy', 'jl940630')
    // console.dir(user)
}