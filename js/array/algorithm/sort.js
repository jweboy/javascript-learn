function ArraySort(array) {
    this.array = [...array]
}

ArraySort.prototype = {
    swap(index1, index2) {
        const tmp = this.array[index2]
        this.array[index2] = this.array[index1]
        this.array[index1] = tmp
    },
    /**
     * @name 冒泡排序
     * @description 比较两个前后相邻的元素,如果第一个比第二个大,则交换它们的位置。时间复杂度为O(n2)。
     * @param {Array} originArray 源数组
     */
    bubbleSort() {
        let swapped = false
    
        for (let i = 0; i < this.array.length; i++) {
            // 每一轮循环初始化
            swapped = false
    
            // 每一轮循环完成一次排序,其中一个元素会排列到正确的位置上，那么剩余循环次数会依次减少一次
            for (let j = 0; j < this.array.length - i; j++) {
                if(this.array[j] > this.array[j + 1]) {
                    this.swap(j, j + 1)
                }
                swapped = true
            }
    
            // 如果数组已经排序直接返回
            if(!swapped) {
                return this.array
            }
        }
        return this.array
    },
    /**
     * @name 选择排序(原址比较)
     * @description 找到最小值并将其放置在第一位,接着找到第二小的值并将其放在第二位,以此类推。时间复杂度为O(n2)。
     */
    selectionSort() {
        for (let i = 0; i < this.array.length; i++) {
            // 记录每次循环外部的index位置
            let minIndex = i
            
            // 每一轮循环完成一次排序,其中一个元素会排列到正确的位置上，即依次遍历剩余未排序的元素
            for (let j = i + 1; j < this.array.length; j++) {
                // 如果每轮循环内部index小于外部，那么内部index作为最小索引位置
                if(this.array[j] < this.array[minIndex]) {
                    minIndex = j
                }
            }
            
            // 如果每轮循环内外两次index不一致，则交换位置。
            if(minIndex !== i) {
                this.swap(minIndex, i)
            }
        }
        return this.array
    },
    /**
     * @name 插入排序(较稳定)
     * @description 每一步将一个待排序的元素，按照它值的大小插入前面已经排序的数组中适当位置上，直到全部插入完为止。
     */
    insertionSort() {
        for (let i = 0; i < this.array.length; i++) {
            // 保存当前的index
            let currentIndex = i
            
            while(
                // 当前不是第一个元素
                this.array[currentIndex -1] !== undefined && 
                (this.array[currentIndex] < this.array[currentIndex - 1])
            ) {
                this.swap(currentIndex, currentIndex - 1)

                // 元素前移之后，保证index位置准确
                currentIndex -= 1
            }
        } 

        return this.array
    }
}



// const array = [20, 4, 12]
const array = [90, 32, 222, 4, 50, 1]
const arraySortMethod = new ArraySort(array)
console.log(arraySortMethod.insertionSort(array))