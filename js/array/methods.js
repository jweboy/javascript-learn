// 类数组对象转换为数组
Array.from({ a: '1', b: 'b', length: 2 }) // ['undefined', 'undefined']
Array.from({ '0': 'a', '1': 'b', length: 2 }) // ['a', 'b']

Array.of(1,2,3,4,5) // [1,2,3,4,5]